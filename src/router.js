import Vue from 'vue'
import Router from 'vue-router'

// Routes
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import About from './views/About.vue'
import Settings from './views/Settings.vue'
import Admin from './views/Admin.vue'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/settings',
            name: 'settings',
            component: Settings
        },
        {
            path: '/admin',
            name: 'admin',
            component: Admin
        },
    ]
})
