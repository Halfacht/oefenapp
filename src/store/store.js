import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex);

const vuexPersist = new VuexPersist({
    key: 'oefenapp',
    sotrage: localStorage
});

export const store = new Vuex.Store({
    plugins: [vuexPersist.plugin],
    state: {
        klassen: [
            {
                naam: 'Klas 1',
                active: true,
                leerlingen: {
                    Test: {
                        naam: 'Test',
                        optellen: true,
                        aftrekken: true,
                        vermenigvuldigen: true,
                        delen: true,
                        min: 1,
                        max: 10,
                        aantal: 30,
                        gemaakt: 5,
                        goed: 3,
                        som: '',
                        operatorKey: ''
                    }
                },
                optellen: {
                    aantal: 134,
                    goed: 120,
                },
                aftrekken: {
                    aantal: 120,
                    goed: 87
                },
                vermenigvuldigen: {
                    aantal: 63,
                    goed: 34
                },
                delen: {
                    aantal: 100,
                    goed: 45,
                },
            },
        ],
    },
    mutations: {
        createLeerling(state, payload) {
            state.klassen[payload.index].leerlingen[payload.naam] = {
                naam: payload.naam,
                optellen: true,
                aftrekken: true,
                vermenigvuldigen: true,
                delen: true,
                min: 1,
                max: 100,
                aantal: 20,
                gemaakt: 0,
                goed: 0,
                som: '',
                operatorKey: ''
            };
        },
        createKlas(state, klassenNaam) {
            state.klassen.push(
                {
                    naam: klassenNaam,
                    active: true,
                    leerlingen: [],
                    optellen: {
                        aantal: 0,
                        goed: 0,
                    },
                    aftrekken: {
                        aantal: 0,
                        goed: 0
                    },
                    vermenigvuldigen: {
                        aantal: 0,
                        goed: 0
                    },
                    delen: {
                        aantal: 0,
                        goed: 0,
                    },
                }
            );
        },
        updateLeerling(state, leerling) {
            // todo: DRY
            state.klassen[store._vm.$session.get('klasIndex')].leerlingen[store._vm.$session.get('userName')] = leerling;
        },
        somGemaakt(state) {
            // todo: DRY
            state.klassen[store._vm.$session.get('klasIndex')].leerlingen[store._vm.$session.get('userName')].gemaakt++;
            state.klassen[store._vm.$session.get('klasIndex')][state.klassen[store._vm.$session.get('klasIndex')].leerlingen[store._vm.$session.get('userName')].operatorKey].aantal++;
        },
        somGoed(state) {
            // todo: DRY
            state.klassen[store._vm.$session.get('klasIndex')].leerlingen[store._vm.$session.get('userName')].goed++
        },
        updateSom(state, som) {
            // todo: DRY
            state.klassen[store._vm.$session.get('klasIndex')].leerlingen[store._vm.$session.get('userName')].som = som.som;
            state.klassen[store._vm.$session.get('klasIndex')].leerlingen[store._vm.$session.get('userName')].operatorKey = som.operatorKey;
        },
        updateActive(state, payload) {
            // todo: DRY
            state.klassen.find(klas => klas.naam === payload.klasNaam).active = payload.active;
        }
    },
    getters: {
        getLoggedInLeerling: state => {
            return state.klassen[store._vm.$session.get('klasIndex')].leerlingen[store._vm.$session.get('userName')];
        },
        getKlasByNaam: (state) => (naam) => {
            return state.klassen.find(klas => klas.naam === naam);
        },
        getActiveKlassen: (state) => {
            return state.klassen.filter(klas => klas.active);
        }
    }
});